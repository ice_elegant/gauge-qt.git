#include "CProgressOne.h"
#include "ui_CProgressOne.h"

CProgressOne::CProgressOne(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CProgressOne)
{
    ui->setupUi(this);

    setValue(500);
    setMinValue(0);
    setMaxValue(500);
    setUnit("ug/m3");
    setLevelText("轻度");
    setLevelColor(QColor(74, 220, 115));
    setBgColor(QColor(115, 172, 249));
    setBorderColor(QColor(31, 223, 119));
    //setBorderColor(QColor(Qt::yellow));
    setTextColor(Qt::white);    
    setBarColor(QColor(Qt::yellow));
    radius = 2;
    borderWidth = 0;
    autoRadius = true;
    autoFont = true;
    showValue = true;
    showPercent = false;
    m_barY = 20;
    m_offsetMaxmin = 10;
}

CProgressOne::~CProgressOne()
{
    delete ui;
}

void CProgressOne::paintEvent(QPaintEvent *)
{

    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    if(autoRadius)
    {
        radius = barHeight / 2;
    }
    barHeight = this->width() / 18;
    m_barY = barHeight*4;

    drawBg(&painter);
    drawValue(&painter);
    //drawBorder(&painter);
    drawText(&painter);   
    drawLevel(&painter);
}

void CProgressOne::drawBg(QPainter *painter)
{
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(bgColor);    
    painter->drawRoundedRect(QRect(0, m_barY, this->width(), barHeight), radius, radius);
    painter->restore();
}

void CProgressOne::drawValue(QPainter *painter)
{
    painter->save();
    // 计算百分比及对应的宽度   
    double percent = (double)m_value / (minValue + maxValue);
    if(percent < 0 || percent > 1)
    {
        if(percent < 0)
        {
            percent = 0.15;
            setLevelText("优");
            setLevelColor(QColor("#1fdf76"));
        }
        else
        {
            percent = 1;
        }
    }
    width1 = this->width() * percent;

    painter->setPen(Qt::NoPen);
    painter->setBrush(m_barColor);
/**/
    // 计算绘制的区域，需要裁剪圆角部分
    QPainterPath clipPath;
    //radius = 20;
    clipPath.addRoundedRect(QRect(0, m_barY, this->width(), barHeight), radius, radius);
    painter->setClipPath(clipPath);
    QRect rect(0, m_barY, width1, barHeight);
    painter->drawRect(rect);
    //painter->drawRoundRect(0, m_barY, width1, barHeight, 80, 80);
/**/
    painter->restore();
}

void CProgressOne::drawBorder(QPainter *painter)
{
    painter->save();

    QPen pen;
    pen.setWidthF(borderWidth);
    pen.setColor(borderColor);
    painter->setPen(borderWidth > 0 ? pen :Qt::NoPen);
    painter->setBrush(Qt::NoBrush);

    // 绘制圆角
    painter->drawRoundedRect(this->rect(), radius, radius);

    painter->restore();
}

void CProgressOne::drawText(QPainter *painter)
{
    painter->save();

    //QRect rect(0, 0, width1, this->height());
    QRect rect(0, 0, this->width() * 0.5, this->height());
    QFont font;
    // 绘制文字

    // 设置字体和颜色
    if(autoFont)
    {
        font.setPixelSize(barHeight*3); //this->height() * 0.9);
        painter->setFont(font);
    }

    QString text;
    if(m_value >= minValue)
    {
        text = QString::number(m_value);
    }
    else
    {
        text = "--";
    }
    //if(showPercent)
    //{
        //double percent = (double)m_value / (maxValue - minValue);
        //text = QString("%1%").arg(QString::number(percent*100, 'f', 0));
    //}
    painter->setPen(textColor);
    painter->drawText(rect, Qt::AlignTop, text);


    font.setPixelSize(barHeight*1.3);
    painter->setFont(font);
    painter->drawText(QRect(0, barHeight*1.5 + m_barY, this->width(), barHeight*2), Qt::AlignLeft, QString::number(minValue));
    painter->drawText(QRect(0, barHeight*1.5 + m_barY, this->width(), barHeight*2), Qt::AlignRight, QString::number(maxValue));

    // 单位
    painter->drawText(QRect(0, barHeight*1.2, this->width(), barHeight*2), Qt::AlignCenter, m_unit);

    painter->restore();
}

void CProgressOne::drawLevel(QPainter *painter)
{
    painter->save();

    painter->setPen(Qt::NoPen);

    painter->setBrush(m_levelColor);
    QRect rect(this->width() - barHeight * 4, barHeight*0.6, barHeight*4, barHeight*2.5);
    // 绘制圆角
    painter->drawRoundedRect(rect, radius, radius);

    painter->restore();

    //
    painter->save();

    QFont font;
    font.setPixelSize(barHeight*1.5);
    painter->setPen(textColor);
    painter->setFont(font);
    painter->drawText(rect, Qt::AlignCenter, m_levelText);

    painter->restore();

}

int CProgressOne::getValue() const
{
    return m_value;
}

int CProgressOne::getMinValue() const
{
    return minValue;
}

int CProgressOne::getMaxValue() const
{
    return maxValue;
}

QString CProgressOne::getUnit() const
{
    return m_unit;
}

QString CProgressOne::getLevelText() const
{
    return m_levelText;
}

QColor CProgressOne::getLevelColor() const
{
    return m_levelColor;
}

QColor CProgressOne::getBarColor() const
{
    return m_barColor;
}

int CProgressOne::getRadius() const
{
    return radius;
}

bool CProgressOne::getAutoRadius() const
{
    return autoRadius;
}

bool CProgressOne::getShowValue() const
{
    return showValue;
}

bool CProgressOne::getShowPercent() const
{
    return showPercent;
}

bool CProgressOne::getAutoFont() const
{
    return autoFont;
}

double CProgressOne::getBorderWidth() const
{
    return borderWidth;
}

QColor CProgressOne::getBorderColor() const
{
    return borderColor;
}

QColor CProgressOne::getBgColor() const
{
    return bgColor;
}

QColor CProgressOne::getTextColor() const
{
    return textColor;
}

QSize CProgressOne::sizeHint() const
{

}

QSize CProgressOne::minimumSizeHint() const
{

}

void CProgressOne::setValue(int value)
{
    m_value = value;
    this->update();
}

void CProgressOne::setMinValue(int value)
{
    minValue = value;
    this->update();
}

void CProgressOne::setMaxValue(int value)
{
    maxValue = value;
    this->update();
}

void CProgressOne::setUnit(QString unit)
{
    if(m_unit != unit)
    {
        m_unit = unit;
        this->update();
    }
}

void CProgressOne::setLevelText(QString text)
{
    m_levelText = text;
}

void CProgressOne::setLevelColor(const QColor &color)
{
    m_levelColor = color;
    m_barColor = color;
    this->update();
}

void CProgressOne::setBarColor(const QColor &color)
{
    m_barColor = color;
    this->update();
}

void CProgressOne::setRadius(int radius)
{
    this->radius = radius;
    this->update();
}

void CProgressOne::setAutoRadius(bool autoRadius)
{
    this->autoRadius = autoRadius;
    this->update();
}

void CProgressOne::setShowValue(bool showValue)
{
    this->showValue = showValue;
    this->update();
}

void CProgressOne::setShowPercent(bool showPercent)
{
    this->showPercent = showPercent;
    this->update();
}

void CProgressOne::setAutoFont(bool autoFont)
{
    this->autoFont = autoFont;
    this->update();
}

void CProgressOne::setBorderWidth(double borderWidth)
{
    this->borderWidth = borderWidth;
    this->update();
}

void CProgressOne::setBorderColor(const QColor &borderColor)
{
    this->borderColor = borderColor;
    this->update();
}

void CProgressOne::setBgColor(const QColor &bgColor)
{
    this->bgColor = bgColor;
    this->update();
}

void CProgressOne::setTextColor(const QColor &textColor)
{
    this->textColor = textColor;
    this->update();
}
