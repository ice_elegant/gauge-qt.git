HEADERS     += \
    $$PWD/CProgressOne.h \
    $$PWD/CProgressWater.h \
    $$PWD/CGaugeSemiCircle.h


SOURCES     += \
    $$PWD/CProgressOne.cpp \
    $$PWD/CProgressWater.cpp \
    $$PWD/CGaugeSemiCircle.cpp

FORMS       += \
    $$PWD/CProgressOne.ui \
    $$PWD/CProgressWater.ui \
    $$PWD/CGaugeSemiCircle.ui
RESOURCES   += 

