#ifndef CPROGRESSONE_H
#define CPROGRESSONE_H

#include <QWidget>
//#include <QtDesigner/QDesignerExportWidget>
#include <QPainter>
#include <QDebug>

namespace Ui {
class CProgressOne;
}

class CProgressOne : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int m_value READ getValue WRITE setValue)
    Q_PROPERTY(int minValue READ getMinValue WRITE setMinValue)
    Q_PROPERTY(int maxValue READ getMaxValue WRITE setMaxValue)
    Q_PROPERTY(QString m_unit READ getUnit WRITE setUnit)
    Q_PROPERTY(QString  levelText READ getLevelText WRITE setLevelText)
    Q_PROPERTY(QColor levelColor READ getLevelColor WRITE setLevelColor)

    Q_PROPERTY(QColor m_barColor READ getBarColor WRITE setBarColor)

    Q_PROPERTY(int radius READ getRadius WRITE setRadius)
    Q_PROPERTY(bool autoRadius READ getAutoRadius WRITE setAutoRadius)

    Q_PROPERTY(bool showValue READ getShowValue  WRITE setShowValue)
    Q_PROPERTY(bool showPercent READ getShowPercent WRITE setShowPercent)
    Q_PROPERTY(bool autoFont READ getAutoFont WRITE setAutoFont)

    Q_PROPERTY(double borderWidth READ getBorderWidth WRITE setBorderWidth)
    Q_PROPERTY(QColor borderColor READ getBorderColor WRITE setBorderColor)

    Q_PROPERTY(QColor bgColor READ getBgColor WRITE setBgColor)
    Q_PROPERTY(QColor textColor READ getTextColor WRITE setTextColor)

public:
    explicit CProgressOne(QWidget *parent = 0);
    ~CProgressOne();

protected:
    void paintEvent(QPaintEvent *);
    void drawBg(QPainter *painter);
    void drawValue(QPainter *painter);
    void drawBorder(QPainter *painter);
    void drawText(QPainter *painter);
    void drawLevel(QPainter *painter);

private:
    Ui::CProgressOne *ui;

    int m_value;
    int minValue;
    int maxValue;
    QString m_unit;
    QString m_levelText;

    int m_textY;
    int m_barY;
    int m_offsetMaxmin;
    int m_rangeY;

    QColor m_barColor;
    QColor m_levelColor;

    int radius;
    bool autoRadius;

    bool showValue;
    bool showPercent;
    bool autoFont;

    double borderWidth;
    QColor borderColor;

    QColor bgColor;
    QColor textColor;

    int width1;
    int barHeight;

public:
    int getValue() const;
    int getMinValue() const;
    int getMaxValue() const;
    QString getUnit() const;
    QString getLevelText() const;
    QColor getLevelColor() const;


    QColor getBarColor() const;

    int getRadius() const;
    bool getAutoRadius() const;

    bool getShowValue() const;
    bool getShowPercent() const;
    bool getAutoFont() const;

    double getBorderWidth() const;
    QColor getBorderColor() const;

    QColor getBgColor() const;
    QColor getTextColor() const;

    QSize sizeHint() const;
    QSize minimumSizeHint() const;

public Q_SLOTS:
    void setValue(int value);
    void setMinValue(int value);
    void setMaxValue(int value);
    void setUnit(QString unit);
    void setLevelText(QString text);
    void setLevelColor(const QColor &color);


    void setBarColor(const QColor &color);
    void setRadius(int radius);
    void setAutoRadius(bool autoRadius);

    void setShowValue(bool showValue);
    void setShowPercent(bool showPercent);
    void setAutoFont(bool autoFont);

    void setBorderWidth(double borderWidth);
    void setBorderColor(const QColor &borderColor);

    void setBgColor(const QColor &bgColor);
    void setTextColor(const QColor &textColor);

};

#endif // CPROGRESSONE_H
